
//
//  MemoListViewModel.swift
//  Memo
//
//  Created by Girish Koundinya on 12/10/18.
//  Copyright © 2018 Koundinya. All rights reserved.
//

import Foundation

class MemoListViewModel {
    
    var sectionsDataSource : [String]
    var rowDataSource : [String : [Memo]]
    init(){
        sectionsDataSource = []
        rowDataSource = [String : [Memo]]()
    }

    func filterMemos(data : NSArray,completion:@escaping () -> Void){
        sectionsDataSource.removeAll()
        rowDataSource.removeAll()
        var memoObjects  : [Memo] = []
        data.forEach({ (dataObj) in
            let memo = dataObj as! NSDictionary
            do{
                let memoObject = try Memo(title: memo.object(forKey: "title") as! String, notes:memo.object(forKey: "note") as! String,created: ViewHelper.convertToDate(dateString: memo.object(forKey: "created_at") as! String))
                if let part = memo.object(forKey: "participants") as! NSString?{
                    memoObject.Participants = part as String
                }
                self.rowDataSource.updateValue([], forKey: ViewHelper.convertToString(theDate: memoObject.CreatedAt))
                memoObjects.append(memoObject)
            }catch let error {
                
            }
        })
        for (key,_) in self.rowDataSource{
            self.sectionsDataSource.append(key)
            for mem in memoObjects {
                if key == ViewHelper.convertToString(theDate: mem.CreatedAt) {
                    self.rowDataSource[key]?.append(mem)
                }
            }
        }
        completion()
    }
    
    func fetchMemos(completion:@escaping () -> Void){
        sectionsDataSource.removeAll()
        rowDataSource.removeAll()
        API.GetMemos { (data) in
            var memoObjects  : [Memo] = []
            data.forEach({ (dataObj) in
                let memo = dataObj as! NSDictionary
                do{
                    let memoObject = try Memo(title: memo.object(forKey: "title") as! String, notes:memo.object(forKey: "note") as! String,created: ViewHelper.convertToDate(dateString: memo.object(forKey: "created_at") as! String))
                    if let part = memo.object(forKey: "participants") as! NSString?{
                        memoObject.Participants = part as String
                    }
                    self.rowDataSource.updateValue([], forKey: ViewHelper.convertToString(theDate: memoObject.CreatedAt))
                    memoObjects.append(memoObject)
                }catch let error {
                    
                }
            })
            for (key,_) in self.rowDataSource{
                self.sectionsDataSource.append(key)
                for mem in memoObjects {
                    if key == ViewHelper.convertToString(theDate: mem.CreatedAt) {
                        self.rowDataSource[key]?.append(mem)
                    }
                }
            }
            completion()
        }
    }
    
    
}
