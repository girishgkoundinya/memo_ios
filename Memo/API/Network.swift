//
//  Network.swift
//  Memo
//
//  Created by Girish Koundinya on 14/10/18.
//  Copyright © 2018 Koundinya. All rights reserved.
//

import Foundation
import Alamofire


class API{
    
    static let baseURLString = "https://us-central1-memo-b29dc.cloudfunctions.net/"
//    static let baseURLString = "http://localhost:5000/memo-b29dc/us-central1/"
    static func GetMemos(completion: @escaping (_ result:NSArray) -> Void){
        let requestString = baseURLString + "GetAllMemos"
        Alamofire.request(requestString, method: .get).responseJSON { response in
            if let json = response.result.value {
                let jsonResponse = json as! NSDictionary
                completion(jsonResponse.object(forKey: "data") as! NSArray)
            }
        }
    }
    static func CreateMemo(params:[String: String],completion: @escaping(_ result: Bool) -> Void){
        let requestString = baseURLString + "CreateMemos"
        Alamofire.request(requestString, method: .post, parameters: params,encoding: JSONEncoding.default, headers: nil).responseJSON {response in
            if response.result.value != nil {
                completion(true)
            }else{
                completion(false)
            }
        }
    }
    
    static func FilterMemos(query: String, completion: @escaping (_ result:NSArray) -> Void){
        let requestString = baseURLString + "FilterMemos" + query
        Alamofire.request(requestString, method: .get).responseJSON { response in
            if let json = response.result.value {
                let jsonResponse = json as! NSDictionary
                completion(jsonResponse.object(forKey: "data") as! NSArray)
            }
        }
        
    }
}


