//
//  Memo.swift
//  Memo
//
//  Created by Girish Koundinya on 14/10/18.
//  Copyright © 2018 Koundinya. All rights reserved.
//

import Foundation


class Memo {
    var Title : String
    var Note : String
    var Participants : String?
    var CreatedAt : Date
    
    init(title:String, notes:String, created:Date){
        self.Title = title
        self.Note = notes
        self.CreatedAt = created
    }
    
    init(title:String, notes:String, people:String, created:Date){
            self.Title = title
            self.Note = notes
            self.Participants = people
            self.CreatedAt = created
    }
}

