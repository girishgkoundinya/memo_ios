//
//  MemoDetailViewController.swift
//  Memo
//
//  Created by Girish Koundinya on 12/10/18.
//  Copyright © 2018 Koundinya. All rights reserved.
//

import Foundation
import UIKit
import Eureka
import ImageRow

class MemoDetailViewController : FormViewController {
    var memoObject : Memo? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        initForm()
    }
    
    private func initForm(){
        let section = Section(){ section in
            var header = HeaderFooterView<PaddingLabel>(.class)
            header.height = { 50.0 }
            header.onSetupView = {view, _ in
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MMM/yyyy"
                let dateString = dateFormatter.string(from:(self.memoObject?.CreatedAt)!)
                view.text = dateString
                view.font = UIFont.boldSystemFont(ofSize:16)
            }
            section.header = header
        }
        form +++ section
        section
            <<< TextRow() { row in
                row.title = "Title :"
                row.value = memoObject?.Title
                row.cell.isUserInteractionEnabled = false
                }.cellUpdate { cell, row in
                    cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
                    cell.textField.textAlignment = .left
                }
            <<< TextAreaRow(){ row in
                row.title = "Meeting Notes :"
                row.value = memoObject?.Note
                row.cell.height = {
                    return 250.0
                }
                row.cell.isUserInteractionEnabled = false
                
            }
            <<< NameRow() {
                $0.title = "With :"
                $0.value = memoObject?.Participants
                $0.cell.isUserInteractionEnabled = false
                }.cellUpdate { cell, row in
                    cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
                    cell.textField.textAlignment = .left
                }
            <<< ImageRow() { row in
                row.placeholderImage = UIImage.init(named: "UIBarButtonSystemItemCamera")
                row.title = "Photo"
                row.sourceTypes = [.PhotoLibrary, .SavedPhotosAlbum]
                row.clearAction = .no
        }
    }
}
