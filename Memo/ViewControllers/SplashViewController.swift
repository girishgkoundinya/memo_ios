//
//  SplashViewController.swift
//  Memo
//
//  Created by Girish Koundinya on 12/10/18.
//  Copyright © 2018 Koundinya. All rights reserved.
//

import Foundation
import UIKit
import Lottie

class SplashScreenViewController : UIViewController {
    
    @IBOutlet weak var animationView: LOTAnimationView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initTransition()
    }

    private func initTransition(){
        animationView.setAnimation(named: "loader")
        animationView.play { (complete) in
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.initIndex()
        }
    }
}
