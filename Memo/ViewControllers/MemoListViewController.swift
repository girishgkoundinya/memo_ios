//
//  MemoListViewController.swift
//  Memo
//
//  Created by Girish Koundinya on 12/10/18.
//  Copyright © 2018 Koundinya. All rights reserved.
//

import Foundation
import UIKit
import STPopup

class MemoNavViewController : UINavigationController {}

protocol MemoFilter:class{
    func filterMemos(filtered: NSArray)
}

protocol MemoListRefresh:class {
    func didCreateMemo()
}

class MemoListViewController : UIViewController, UITableViewDelegate, UITableViewDataSource,MemoListRefresh,MemoFilter {

    func filterMemos(filtered: NSArray) {
        viewModel.filterMemos(data: filtered) {
            self.memoList.reloadData()
        }
    }
    
    func didCreateMemo() {
        fetchMemos()
    }

    
    var viewModel : MemoListViewModel = MemoListViewModel()
    var popController : STPopupController = STPopupController()
    
    let cellIdentifier = "memoListCell"
    
    @IBOutlet weak var memoList: UITableView!
    override func viewDidLoad() {
        initNavBar()
        initTableView()
        super.viewDidLoad()
        fetchMemos()
    }
    
    private func fetchMemos(){
        viewModel.fetchMemos {
            self.memoList.reloadData()
        }
    }
    
    private func initNavBar(){
        self.navigationItem.title = "My Memos"
        navigationController?.navigationBar.prefersLargeTitles = true
        let searchMemo = UIBarButtonItem.init(barButtonSystemItem: .search, target: self, action: #selector(MemoListViewController.searchMemo(_:)))
        searchMemo.tintColor = UIColor.black
        let newMemo = UIBarButtonItem.init(barButtonSystemItem: .add, target: self, action: #selector(MemoListViewController.createMemo(_:)))
        newMemo.tintColor = UIColor.black
        
        let refreshList = UIBarButtonItem.init(barButtonSystemItem: .refresh, target: self, action: #selector(MemoListViewController.presentSuccess))
        refreshList.tintColor = UIColor.black

        self.navigationItem.rightBarButtonItems = [searchMemo,newMemo,refreshList]
    }
    
    @objc func presentSuccess(){
        fetchMemos()
    }
    
    @objc private func createMemo(_ sender:UIBarButtonItem!){
        let newMemoNav = ViewHelper.MainStoryboard().instantiateViewController(withIdentifier: "NewMemoNav")
        let newMemo : NewMemoViewController = newMemoNav.childViewControllers[0] as! NewMemoViewController
        newMemo.delegate = self
        self.navigationController?.present(newMemoNav, animated: true, completion:nil)
    }
    
    @objc private func searchMemo(_ sender:UIBarButtonItem!){
        
        let searchMemo  : SearchMemoViewController = ViewHelper.MainStoryboard().instantiateViewController(withIdentifier: "SearchMemo") as! SearchMemoViewController
        searchMemo.delegate = self;
        searchMemo.contentSizeInPopup = CGSize(width: self.view!.frame.width, height: 350)
        popController = STPopupController(rootViewController: searchMemo)
        let tap = UITapGestureRecognizer(target: self, action: #selector(MemoListViewController.handleTap(_:)))
        popController.backgroundView?.addGestureRecognizer(tap)
        popController.containerView.layer.cornerRadius = 20.0
        popController.navigationBar.tintColor = UIColor.black
        popController.style = STPopupStyle.bottomSheet
        popController.present(in: self)
    }

    @objc func handleTap(_ sender:UITapGestureRecognizer){
       popController.backgroundView?.gestureRecognizers = nil
       popController.topViewController?.dismiss(animated: true, completion: nil)
    }
    
    
    private func initTableView(){
        memoList.tableFooterView = UIView()
        memoList.dataSource = self
        memoList.delegate = self
    }

    @objc private func refresh(sender:AnyObject) {
        self.fetchMemos()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sectionsDataSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.rowDataSource.count > 0{
            let section = viewModel.sectionsDataSource[section]
            return (viewModel.rowDataSource[section]?.count)!
        }
        return 0
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailViewController : MemoDetailViewController = ViewHelper.MainStoryboard().instantiateViewController(withIdentifier: "MemoDetail") as! MemoDetailViewController
        let section = viewModel.sectionsDataSource[indexPath.section]
        let memo : Memo = (viewModel.rowDataSource[section])![indexPath.item]
        detailViewController.memoObject = memo
        navigationController?.navigationBar.tintColor = .black
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier, for: indexPath)
        let section = viewModel.sectionsDataSource[indexPath.section]
        let memo : Memo = (viewModel.rowDataSource[section])![indexPath.item]
        cell.textLabel?.text = memo.Title
        cell.detailTextLabel?.text = memo.Participants
        cell.selectionStyle = .none
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.groupTableViewBackground
        let headerLabel = UILabel(frame: CGRect(x: 15, y: 10, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.text = viewModel.sectionsDataSource[section]
        headerLabel.font = UIFont.boldSystemFont(ofSize:16)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

}
