//
//  SearchMemoViewController.swift
//  Memo
//
//  Created by Girish Koundinya on 12/10/18.
//  Copyright © 2018 Koundinya. All rights reserved.
//

import Foundation
import UIKit
import Eureka
import SplitRow


class SearchMemoViewController : FormViewController {

    weak var delegate: MemoFilter?

    override func viewDidLoad() {
        super.viewDidLoad()
        initNavBar()
        initForm()
    }

    private func initNavBar(){
        self.title = ""
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(SearchMemoViewController.search(_:)))
    }
    
    
    @objc private func search(_ sender:UIBarButtonItem!){
        
        var queryParam : String = "?"
        queryParam += "note_query="
        if let pString = form.values()["note"] as? String {
            queryParam += pString
        }
        queryParam += "&participants_query="
        if let pString = form.values()["participants"] as? String {
            queryParam += pString
        }

        let row = form.rowBy(tag: "between") as? SplitRow<DateRow,DateRow>
        
        queryParam += "&fromDate="
        if let dString = row?.value?.left as? Date {
            queryParam += ViewHelper.convertToString(theDate: dString)
        }

        queryParam += "&toDate="
        if let dString = row?.value?.right as? Date {
            queryParam += ViewHelper.convertToString(theDate: dString)
        }

        
        API.FilterMemos(query: queryParam) { (data) in
            self.delegate?.filterMemos(filtered: data)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func initForm(){
        let section = Section(){ section in
            var header = HeaderFooterView<PaddingLabel>(.class)
            header.height = { 0 }
            section.header = header
        }
        form +++ section
            <<< NameRow(){ row in
                row.title = "Participant"
                row.tag = "participants"
                row.placeholder = "Enter name here"
            }
            <<< SplitRow<DateRow,DateRow>(){
                $0.tag = "between"
                $0.rowLeftPercentage = 0.5
                $0.rowLeft = DateRow(){
                    $0.title = "From"
                    let currentDate = Date()
                    $0.value = currentDate
                    $0.tag = "from"
                }
                $0.rowRight = DateRow(){
                    $0.title = "To"
                    let currentDate = Date()
                    $0.value = currentDate
                    $0.tag = "to"
                }
            }
            <<< TextRow(){ row in
                row.title = "Contains"
                row.tag = "note"
                row.placeholder = "Enter search text here"
            }
            <<< ButtonRow() {
            $0.title = "Search"
            }
            .onCellSelection {  cell, row in
                self.search(nil)
            }
    }
    
}
