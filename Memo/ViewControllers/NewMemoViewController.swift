//
//  NewMemoViewController.swift
//  Memo
//
//  Created by Girish Koundinya on 12/10/18.
//  Copyright © 2018 Koundinya. All rights reserved.
//

import Foundation
import UIKit
import Eureka
import ImageRow

class NewMemoViewController : FormViewController {

    weak var delegate: MemoListRefresh?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initForm()
    }
    
    private func initView(){
        self.navigationItem.title = "Add Memo"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        let cancelNew = UIBarButtonItem.init(barButtonSystemItem: .stop, target: self, action:#selector(NewMemoViewController.cancel(_:)))
        cancelNew.tintColor = UIColor.black
        self.navigationItem.leftBarButtonItem = cancelNew
        
        let save = UIBarButtonItem.init(barButtonSystemItem: .save, target: self, action:#selector(NewMemoViewController.save(_:)))
        save.tintColor = UIColor.black
        self.navigationItem.rightBarButtonItem = save
        
        
    }
    
    @objc private func save(_ sender:UIBarButtonItem!){
        let error = form.validate(includeHidden: false)
        if error.count > 0 {
            showError(msg: "Please fill Title and Meeting notes to save Memo")
        }else {
            sendForm()
        }
    }
    
    private func sendForm(){
        var dataFromForm = [String : String]()
        dataFromForm["title"] = form.values()["title"] as? String
        dataFromForm["note"] = (form.values()["notes"] as! String)
        dataFromForm["participants"] = (form.values()["participants"] as? String)
        API.CreateMemo(params: dataFromForm) { (response) in
            if response == true{
                self.delegate?.didCreateMemo()
                self.dismiss(animated: true, completion:nil)
            }else{
                self.showError(msg: "Unable to create a memo now, Please try again later!")
            }
        }
    }
    
    
    
    private func showError(msg: String){
        let alert = UIAlertController(title: "Create Memo", message:msg, preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        let when = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: when){
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc private func cancel(_ sender:UIBarButtonItem!){
        self.dismiss(animated: true, completion:nil)
    }
    
    private func initForm(){
        
        let section = Section(){ section in
            var header = HeaderFooterView<PaddingLabel>(.class)
            header.height = { 50.0 }
            header.onSetupView = {view, _ in
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd/MMM/yyyy"
                let dateString = dateFormatter.string(from:Date())
                view.text = dateString
                view.font = UIFont.boldSystemFont(ofSize:16)
            }
            section.header = header
        }
        form +++ section
        section
            <<< TextRow() { row in
                row.placeholder = "Title"
                row.tag = "title"
                row.add(rule: RuleRequired())
                row.validationOptions = .validatesAlways
            }
            <<< TextAreaRow(){ row in
                row.placeholder = "Meeting notes"
                row.tag = "notes"
                row.cell.height = {
                    return 250.0
                }
                row.add(rule: RuleRequired())
                row.validationOptions = .validatesAlways
                
            }
            <<< NameRow() {
                $0.tag = "participants"
                $0.placeholder = "With"
            }
            <<< ImageRow() { row in
                row.placeholderImage = UIImage.init(named: "UIBarButtonSystemItemCamera")
                row.title = "Photo"
                row.sourceTypes = [.PhotoLibrary, .SavedPhotosAlbum]
                row.clearAction = .yes(style: UIAlertActionStyle.destructive)
                
            }
        }
}


