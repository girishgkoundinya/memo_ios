//
//  AppDelegate.swift
//  Memo
//
//  Created by Girish Koundinya on 08/10/18.
//  Copyright © 2018 Koundinya. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

// Transition Methods
    
    func initIndex(){
        let attrs = [
            NSAttributedStringKey.foregroundColor: UIColor.black,
            NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18.0)
        ]
        UINavigationBar.appearance().titleTextAttributes = attrs
        
        let memoList : MemoNavViewController = ViewHelper.MainStoryboard().instantiateViewController(withIdentifier: "MemoNav") as! MemoNavViewController
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window!.rootViewController = memoList
        self.window!.backgroundColor = UIColor.white
        self.window!.makeKeyAndVisible()
    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Fabric.with([Crashlytics.self])
        UINavigationBar.appearance().barTintColor = ViewHelper.TomatoColor()
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [kCTForegroundColorAttributeName:UIColor.white] as [NSAttributedStringKey : Any]
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }


}

